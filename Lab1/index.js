// server side

const express = require("express");
const path = require("path");

// Create the server with node built in http
const app = express();
const server = require("http").createServer(app);
const port = process.env.PORT || 3000;
const io = require("socket.io")(server);

// serves static index.html
app.use(express.static(path.join(__dirname + "/public")));

// io emits events and listens to events
// whenever a client connects to this server, console.log() will run
io.on("connection", (socket) => {
  // listen to message sent by client using socket.on()
  // in our case we sent "chat" from client side
  socket.on("username", (message) => {
    // console.log("From Client: " + message);
    // emits event to all connected clients
    socket.username = message;
    io.emit("username", message);
  });

  socket.on("message", (msg) => {
    io.emit("message", msg, socket.username);
  });
});

// server.listen for socket.io apps, bootstraps server by passing valid port
server.listen(port, () => {
  console.log(`Server running on port: ${port}`);
});
