const socket = io();

const username = document.querySelector(".username");
const input = document.getElementById("name");
const chatWindow = document.querySelector(".chat-window");
const message = document.getElementById("message");
const chatMessage = document.querySelector(".form-message");

// on username submit
username.addEventListener("submit", (e) => {
  e.preventDefault();
  // send value of input field using sockets
  // sends data from client
  socket.emit("username", input.value);
  // input.value = "";
});

chatMessage.addEventListener("submit", (e) => {
  e.preventDefault();
  socket.emit("message-user", input.value);
});

// on message submit
chatMessage.addEventListener("submit", (e) => {
  e.preventDefault();

  socket.emit("message", message.value);
});

const renderUsername = (message) => {
  const div = document.createElement("div");
  div.classList.add("render-user");
  div.innerText = message + " has joined the chat system!";
  chatWindow.appendChild(div);
};

const renderText = (msg, usr) => {
  const div = document.createElement("div");
  div.classList.add("render-message");
  div.innerText = usr + " : " + msg;
  chatWindow.appendChild(div);
};

const x = socket.on("username", (message) => {
  // console.log("From Server: ", message);
  renderUsername(message);
});

socket.on("message", (msg, user) => {
  renderText(msg, user);
});
