const User = require("./userSchema");
const mongoose = require("mongoose");

/**
 * Database Connection
 */
const uri =
  "mongodb+srv://dbUser:0PMBg9VgyWYKqs80@cluster0.hwfwc.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
mongoose
  .connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    retryWrites: true,
  })
  .then((client) => {
    console.log("Database Connected");
  })
  .catch((err) => {
    console.warn(uri);
    console.warn(`Error occurred while connecting to mongodb: ${err}`);
    process.exit(1);
  });

/**
 * Function determines if user parameters are in the database
 * @param {String} username username of registered user
 * @param {String} password password of registered user
 * @returns true if user is found, false otherwise
 */
const loginUser = async (username, password) => {
  if (username == "" || password == "") {
    return false;
  }
  // Attempt to find user with a matching email in the database
  const foundUser = await User.findOne({
    username: username,
    password: password,
  });

  if (foundUser) {
    // If user is found.
    console.log("Found User: " + foundUser);

    return true;
  } else {
    // If the user is NOT found.
    console.log("User not found, returning false");

    return false;
  }
};

const registerUser = async (username, password) => {
  console.log(username, password);
  // Create new user object with credentials.
  const newUser = new User({
    username: username,
    password: password,
  });
  console.log(newUser);

  // Save the user to the database.
  newUser.save();

  return true;
};

// registerUser("Daniel", "d");
// loginUser("s", "d");

module.exports = { loginUser, registerUser };
