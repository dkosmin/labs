const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
  {
    username: String,
    password: String,
  },
  { collection: "insertDB" }
);

module.exports = mongoose.model("user", userSchema);
