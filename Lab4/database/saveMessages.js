const Message = require("./messageSchema");
const mongoose = require("mongoose");

/**
 * Database Connection
 */
const uri =
  "mongodb+srv://dbUser:0PMBg9VgyWYKqs80@cluster0.hwfwc.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
mongoose
  .connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    retryWrites: true,
  })
  .then((client) => {
    console.log("Database Connected");
  })
  .catch((err) => {
    console.warn(uri);
    console.warn(`Error occurred while connecting to mongodb: ${err}`);
    process.exit(1);
  });

const storeMessage = async (receiver, message) => {
  // let timeStamp = Date.now();
  var currentdate = new Date();
  var datetime =
    "Last Sync: " +
    currentdate.getDate() +
    "/" +
    (currentdate.getMonth() + 1) +
    "/" +
    currentdate.getFullYear() +
    " @ " +
    currentdate.getHours() +
    ":" +
    currentdate.getMinutes() +
    ":" +
    currentdate.getSeconds();
  let messageObject = new Message({
    receiver: receiver,
    message: message,
    timeStamp: datetime,
  });

  messageObject.save();

  return true;
};

const loadData = async () => {
  const data = await Message.find({});
  return data;
};

// console.log(loadData());
// storeMessage("daniel", "hello");
module.exports = { storeMessage, loadData };
