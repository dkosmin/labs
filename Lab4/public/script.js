// client side

// npm modules
const socket = io();

const login = document.querySelector(".nameSub");
const username = document.getElementById("name");
const password = document.getElementById("password");
const chatWindow = document.querySelector(".chat-window");
const message = document.getElementById("message");
const chatMessage = document.querySelector(".form-message");
const loginMessage = document.getElementById("text");
const loginContainer = document.getElementById("public-container");
const storeMessage = document.getElementById("submit-message");

//#######################################################
// Handles Button Submissions and assigns socket emission
//#######################################################

// on username submit
login.addEventListener("click", (e) => {
  e.preventDefault();
  // send value of input field using sockets
  // sends data from client to server
  socket.emit("login", username.value, password.value);

  // input.value = "";
});
// on message submit
chatMessage.addEventListener("submit", (e) => {
  e.preventDefault();
  socket.emit("message", message.value);
});

//########################################################
// Functions to Render text, doesn't interact with sockets
//########################################################

/**
 * Printing to localhost:3000 the name of the user that has logged in
 * @param {String} usr - the username that the user typed to login
 * @param {String} pass - password that the user typed to login
 */
const renderUsername = (usr, pass) => {
  const div = document.createElement("div");
  div.classList.add("render-user");
  div.innerText = usr + " has joined the chat system!";
  chatWindow.appendChild(div);
  username.value = "";
  password.value = "";
};
const renderText = (msg, usr) => {
  const div = document.createElement("div");
  div.classList.add("render-message");
  div.innerText = usr + " : " + msg;
  chatWindow.appendChild(div);
  message.value = "";
};
// const renderError = () => {
//   alert("Invalid Username/Password Entered Try Again");
// };

//######################################################
// Renders function depending on socket emission########
//######################################################

// receive parameters from the server side to call with the functions
socket.on("login", (usr, pass) => {
  // console.log(chatHistory);

  // console.log("From Server: ", message);

  renderUsername(usr, pass);
  loginContainer.style.display = "none";
  document.getElementById("register-container").style.display = "none";
  chatMessage.style.display = "block";
  chatWindow.innerHTML =
    "You are successfully logged in! Can start chatting now.<hr />";
  chatWindow.innerHTML += "Chat messages:";
});

socket.on("unauthenticated", () => {
  alert("invalid username and/or password");
});

// receive parameters from the server side to call with the functions
socket.on("message", (msg, user) => {
  renderText(msg, user);
});

// // load chat history
// socket.on("chat-history", (history) => {
//   console.log(typeof history);
// });

const history = document.getElementById("history");
socket.on("history", (e) => {
  e.forEach((element) => {
    let date = new Date(element.timeStamp);
    history.innerHTML += `${date.toLocaleString()} ${element.receiver}: ${
      element.message
    } <br />`;
  });
});
