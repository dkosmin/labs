// server side

const express = require("express");
const { SocketAddress } = require("net");
const path = require("path");

// Create the server with node built in http
const app = express();
const server = require("http").createServer(app);
const port = process.env.PORT || 3000;
const io = require("socket.io")(server);

// serves static index.html
app.use(express.static(path.join(__dirname + "/public")));

// io emits events and listens to events
// whenever a client connects to this server, console.log() will run
io.on("connection", (socket) => {
  // listen to message sent by client using socket.on()
  // in our case we sent "chat" from client side and are receiving it here on the server side with the usr and pass params
  socket.on("login", (usr, pass) => {
    // emits event to all connected clients
    socket.username = usr;
    socket.password = pass;
    if (DataLayer.checkLogin(usr, pass)) {
      socket.emit("login", usr, pass);
    } else {
      socket.emit("unauthenticated");
    }
  });

  socket.on("message", (msg) => {
    io.emit("message", msg, socket.username);
  });
});

// server.listen for socket.io apps, bootstraps server by passing valid port
server.listen(port, () => {
  console.log(`Server running on port: ${port}`);
});

let DataLayer = {
  info: "Data Layer Implementation for Messenger",
  checkLogin(username, password) {
    if (username === "") {
      console.log("Error: missing username");
      return false;
    } else if (username === "daniel" && password === "d") {
      return true;
    } else if (username === "leah" && password === "x") {
      return true;
    } else if (password === "") {
      console.log("Unauthenticated client sent a chat. Supress!");
    }
    console.log(`Successfully received user: ${username} ${password}`);
    return true;
  },
};
