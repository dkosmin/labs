const mongoose = require("mongoose");

const messageSchema = new mongoose.Schema(
  {
    receiver: String,
    message: String,
    timeStamp: Number,
  },
  { collection: "messages" }
);

module.exports = mongoose.model("message", messageSchema);
