// server side

const express = require("express");
const router = express.Router();
const { SocketAddress } = require("net");
const path = require("path");

// Create the server with node built in http
const app = express();
const server = require("http").createServer(app);
const port = process.env.PORT || 3000;
const io = require("socket.io")(server);
const auth = require("./database/auth");
const message = require("./database/saveMessages");
// serves static index.html
app.use(express.static(path.join(__dirname + "/public")));

// io emits events and listens to events
// whenever a client connects to this server, console.log() will run
io.on("connection", (socket) => {
  /**
   * Checks database to see if user is registered. If user is registered, they can use the messenger app, otherwise, they are given an invalid prompt
   */
  socket.on("login", (usr, pass) => {
    // emits event to all connected clients
    socket.username = usr;
    socket.password = pass;

    // emit all chat history to client
    const history = message.loadData();
    history.then((e) => {
      socket.emit("history", e);
    });

    /**
     * emits login socket if user is in database and an error message otherwise
     */
    auth.loginUser(socket.username, socket.password).then((res) => {
      if (res) {
        socket.emit("login", usr, pass);
      } else {
        console.log(`User ${usr} with password ${pass} not found`);
        socket.emit("unauthenticated");
      }
    });
  });

  socket.on("message", (msg) => {
    message.storeMessage(socket.username, msg);
    io.emit("message", msg, socket.username);
  });

  /**
   * socket to receive and send registered username and password
   */
  socket.on("register-user", (username, password) => {
    /**
     * first check if the user is already registered. if they are, emit a socket to give an error for the registration
     */
    auth.loginUser(username, password).then((res) => {
      if (res) {
        socket.emit("error", username, password);
        /**
         * Otherwise, call the register function and add the user to the database
         */
      } else {
        // registers new user to database
        auth.registerUser(username, password).then((res) => {
          socket.emit("register-user", username, password);
        });
      }
    });
  });
});

// server.listen for socket.io apps, bootstraps server by passing valid port
server.listen(port, () => {
  console.log(`Server running on port: ${port}`);
});
