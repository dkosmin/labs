// npm modules
const socket = io();
//########################################
//Register New User#######################
//########################################
const registerUserContainer = document.getElementById("register-container");
const registeredUsername = document.getElementById("register-name");
const registeredPassword = document.getElementById("register-password");
const registerButton = document.getElementById("register-user");

// console.log(registerUserContainer);

registerButton.addEventListener("click", (e) => {
  e.preventDefault();

  socket.emit(
    "register-user",
    registeredUsername.value,
    registeredPassword.value
  );
});

/**
 * Socket executes if a user registering tries to add in a taken username and password
 */
socket.on("error", () => {
  alert("Error, username and password already taken");
});

/**
 * Shows success message for user registering for a new account
 */
socket.on("register-user", (username, password) => {
  alert(
    `New account with username ${username} and password ${password} registered!`
  );
});
